//
//  Answer.swift
//  Who Sings
//
//  Created by Lucio Botteri on 02/05/21.
//

import Foundation

struct Answer: Identifiable, Equatable {
    let id = UUID()
    let title: String
    let isCorrect: Bool
}
