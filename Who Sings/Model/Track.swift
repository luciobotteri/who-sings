//
//  Track.swift
//  Who Sings
//
//  Created by Lucio Botteri on 03/05/21.
//

import Foundation

struct Track: Codable {
    let id: Int
    let name: String
    let artistId: Int
    let artistName: String
    var lyrics: String?
}
