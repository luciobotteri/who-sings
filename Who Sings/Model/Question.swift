//
//  Question.swift
//  Who Sings
//
//  Created by Lucio Botteri on 02/05/21.
//

import Foundation

struct Question {
    var track: Track
    var answers: [Answer]
}
