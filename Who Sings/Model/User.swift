//
//  User.swift
//  Who Sings
//
//  Created by Lucio Botteri on 02/05/21.
//

import Foundation

struct User: Identifiable, Hashable {
    let id = UUID()
    let name: String
    var score = 0
}
