//
//  NetworkLayer.swift
//  Who Sings
//
//  Created by Lucio Botteri on 02/05/21.
//

import Foundation
import Combine
import SwiftyJSON

class NetworkLayer {
    struct Response<T> {
        let value: T
        let response: URLResponse
    }
    struct TracksResponse: Codable {
        let tracks: [Track]
    }
    
    static private var apiKey = "b1e91b2432560aa970517933f5a8b7c7"
    static private let getTracksUrl = URL(string: "https://api.musixmatch.com/ws/1.1/chart.tracks.get")!
    static private let getLyricsUrl = URL(string: "https://api.musixmatch.com/ws/1.1/track.lyrics.get")!
    
    static private func runRaw(_ request: URLRequest) -> AnyPublisher<Response<Data>, Error> {
        return URLSession.shared
            .dataTaskPublisher(for: request)
            .tryMap { result -> Response<Data> in
                return Response(value: result.data, response: result.response)
            }
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    static func getTracks() -> AnyPublisher<TracksResponse, Error> {
        guard var components = URLComponents(url: getTracksUrl, resolvingAgainstBaseURL: true)
        else { fatalError("Couldn't create URLComponents") }
        components.queryItems = [
            URLQueryItem(name: "apikey", value: apiKey),
            URLQueryItem(name: "f_has_lyrics", value: "1")
        ]
        
        let request = URLRequest(url: components.url!)
        
        return runRaw(request)
            .map {
                if let tracksArray = try? JSON(data: $0.value)["message"]["body"]["track_list"].arrayValue {
                    return TracksResponse(tracks: tracksArray.map {
                        let track = $0["track"]
                        return Track(id: track["track_id"].intValue, name: track["track_name"].stringValue, artistId: track["artist_id"].intValue, artistName: track["artist_name"].stringValue)
                    })
                }
                return TracksResponse(tracks: [])
            }
            .eraseToAnyPublisher()
    }
    
    static func getLyrics(for trackID: Int) -> AnyPublisher<String, Error> {
        guard var components = URLComponents(url: getLyricsUrl, resolvingAgainstBaseURL: true)
        else { fatalError("Couldn't create URLComponents") }
        components.queryItems = [
            URLQueryItem(name: "apikey", value: apiKey),
            URLQueryItem(name: "track_id", value: String(trackID))
        ]
        
        let request = URLRequest(url: components.url!)
        
        return runRaw(request)
            .map {
                if let lyrics = try? JSON(data: $0.value)["message"]["body"]["lyrics"]["lyrics_body"].string {
                    return lyrics
                }
                return "Error"
            }
            .eraseToAnyPublisher()
    }
}
