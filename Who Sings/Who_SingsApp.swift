//
//  Who_SingsApp.swift
//  Who Sings
//
//  Created by Lucio Botteri on 02/05/21.
//

import SwiftUI

@main
struct Who_SingsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
