//
//  LeaderboardsView.swift
//  Who Sings
//
//  Created by Lucio Botteri on 02/05/21.
//

import SwiftUI

struct LeaderboardsView: View {
    @ObservedObject private var appData = AppData.shared
    
    var body: some View {
        NavigationView {
            List(appData.users.sorted { $0.score > $1.score }) { user in
                Text(user.name)
                Spacer()
                Text(String(user.score))
            }.navigationTitle("Leaderboards")
        }
    }
}

struct LeaderboardsView_Previews: PreviewProvider {
    static var previews: some View {
        LeaderboardsView()
    }
}
