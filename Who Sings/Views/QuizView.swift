//
//  QuizView.swift
//  Who Sings
//
//  Created by Lucio Botteri on 02/05/21.
//

import SwiftUI

struct QuizView: View {
    @Environment(\.presentationMode) var presentationMode
    @State var timeRemaining: Int
    @State var questionIndex = 0
    @ObservedObject private var appData = AppData.shared
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    var question: Question {
        return appData.questions[questionIndex]
    }
    
    var body: some View {
        VStack {
            HStack {
                Text("\(questionIndex+1)/\(appData.questions.count)")
                    .bold()
                Spacer()
                Button {
                    presentationMode.wrappedValue.dismiss()
                } label: {
                    Text("Quit")
                        .bold()
                        .padding(.horizontal, 12)
                        .padding(.vertical, 6)
                        .background(Color.red)
                        .foregroundColor(.white)
                        .cornerRadius(10)
                }
            }.padding(.vertical, 10)
            Text(question.track.lyrics ?? question.track.name)
                .multilineTextAlignment(.center)
                .frame(maxHeight: 250)
                .padding(.horizontal, 10)
            Spacer()
            ProgressBarView(progress: $timeRemaining)
                .frame(width: 100, height: 100)
            Spacer()
            ForEach(question.answers) { answer in
                Button {
                    if answer.isCorrect {
                        appData.increaseCurrentUserScore()
                    }
                    nextQuestion()
                } label: {
                    Text(answer.title)
                        .bold()
                        .padding()
                        .frame(maxWidth: .infinity)
                        .background(Color.blue)
                        .foregroundColor(.white)
                        .cornerRadius(10)
                }
            }
            Spacer()
            Text("Your score: \(appData.currentUser.score)")
                .bold()
                .padding(.top, 24)
            Spacer()
        }
        .padding(.horizontal, 10)
        .onReceive(timer) { _ in
            if timeRemaining > 0 {
                timeRemaining -= 1
            } else {
                nextQuestion()
            }
        }
    }
    
    func nextQuestion() {
        if questionIndex < appData.questions.count-1 {
            questionIndex += 1
        } else {
            presentationMode.wrappedValue.dismiss()
        }
        timeRemaining = 10
    }
}

struct QuizView_Previews: PreviewProvider {
    @State static var time = 10
    
    static var previews: some View {
        QuizView(timeRemaining: time)
    }
}
