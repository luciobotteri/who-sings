//
//  ProgressBarView.swift
//  Who Sings
//
//  Created by Lucio Botteri on 02/05/21.
//

import SwiftUI

struct ProgressBarView: View {
    @Binding var progress: Int
    
    var color: Color {
        switch progress {
        case 0...3:
            return Color(UIColor.systemRed)
        case 5...10:
            return Color(UIColor.systemGreen)
        default:
            return Color(UIColor.systemOrange)
        }
    }
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(style: StrokeStyle(lineWidth: 10, lineCap: .butt, lineJoin: .round))
                .foregroundColor(color)
                .animation(.easeInOut(duration: 1))
            Circle()
                .trim(from: 0, to: 1-(CGFloat(progress)/10))
                .stroke(style: StrokeStyle(lineWidth: 20, lineCap: .butt, lineJoin: .round))
                .foregroundColor(Color(UIColor.systemBackground))
                .rotationEffect(Angle(degrees: 270))
                .animation(.linear(duration: 1))
            Text(String(progress))
                .bold()
        }
    }
}

struct ProgressBarView_Previews: PreviewProvider {
    @State static var progress = 5
    static var previews: some View {
        ProgressBarView(progress: $progress)
    }
}
