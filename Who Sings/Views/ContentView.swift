//
//  ContentView.swift
//  Who Sings
//
//  Created by Lucio Botteri on 02/05/21.
//

import SwiftUI

enum ActiveSheet: Identifiable {
    case game, changeName
    
    var id: Int {
        hashValue
    }
}

struct ContentView: View {
    @ObservedObject private var appData = AppData.shared
    @State var activeSheet: ActiveSheet?
    
    var body: some View {
        NavigationView {
            VStack {
                HStack {
                    Text("Welcome, \(appData.currentUsername)!")
                        .bold()
                        .padding()
                    Spacer()
                    Button {
                        activeSheet = .changeName
                    } label: {
                        Text("Change user")
                            .bold()
                            .multilineTextAlignment(.center)
                            .padding(.horizontal, 12)
                            .padding(.vertical, 4)
                            .background(Color.blue)
                            .foregroundColor(.white)
                            .cornerRadius(10)
                            .padding()
                    }
                }
                Spacer()
                if appData.isDownloading {
                    ProgressView()
                        .padding(20)
                        .onDisappear {
                            if !appData.questions.map(\.track.lyrics).isEmpty {
                                activeSheet = .game
                            }
                        }
                } else {
                    Button {
                        if !appData.questions.isEmpty {
                            activeSheet = .game
                        } else {
                            appData.getTracks()
                        }
                    } label: {
                        Text("Play now")
                            .bold()
                            .padding()
                            .background(Color.green)
                            .foregroundColor(.white)
                            .cornerRadius(10)
                            .padding()
                    }
                }
                NavigationLink(
                    destination: LeaderboardsView(),
                    label: {
                        Text("Leaderboards")
                            .bold()
                            .padding()
                            .background(Color.blue)
                            .foregroundColor(.white)
                            .cornerRadius(10)
                    })
                Spacer()
            }.navigationTitle("Who Sings 🎤")
        }
        .fullScreenCover(item: $activeSheet) { item in
            switch item {
            case .game:
                QuizView(timeRemaining: 10)
            case .changeName:
                ChangeNameView(text: "User")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
