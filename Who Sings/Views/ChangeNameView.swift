//
//  ChangeNameView.swift
//  Who Sings
//
//  Created by Lucio Botteri on 02/05/21.
//

import SwiftUI

struct ChangeNameView: View {
    @Environment(\.presentationMode) var presentationMode
    @State var text: String
    @ObservedObject private var appData = AppData.shared
    
    var usernameIsValid: Bool {
        return text.trimmingCharacters(in: .whitespacesAndNewlines).count > 2
    }
    var buttonColor: Color {
        return usernameIsValid ? .blue : Color(white: 0.3)
    }
    var buttonTitleColor: Color {
        return usernameIsValid ? .white : Color(white: 0.8)
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Write your name here:")
                .padding([.top, .leading], 16)
            TextField("Username", text: $text)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding([.bottom, .leading, .trailing], 16)
            Spacer()
            Button {
                let newUsername = text.trimmingCharacters(in: .whitespacesAndNewlines)
                if !appData.users.map({$0.name}).contains(newUsername) {
                    appData.users.append(User(name: newUsername))
                }
                appData.currentUsername = newUsername
                presentationMode.wrappedValue.dismiss()
            } label: {
                Text("Confirm")
                    .bold()
                    .frame(maxWidth: .infinity)
                    .padding()
                    .background(buttonColor)
                    .foregroundColor(buttonTitleColor)
                    .cornerRadius(10)
            }
            .disabled(!usernameIsValid)
            .padding()
        }
    }
}

struct ChangeNameView_Previews: PreviewProvider {
    @State static var text = "User"
    static var previews: some View {
        ChangeNameView(text: text)
    }
}
