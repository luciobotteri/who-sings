//
//  AppData.swift
//  Who Sings
//
//  Created by Lucio Botteri on 02/05/21.
//

import Foundation
import SwiftUI
import Combine

class AppData: ObservableObject {
    @Published var users = [User]()
    @Published var questions = [Question]()
    @Published var isDownloading = false
    
    var currentUsername: String
    var currentUser: User {
        return users.first { $0.name == currentUsername }!
    }
    
    static let shared = AppData()
    
    private var cancellationTokens = Set<AnyCancellable>()
    
    init() {
        currentUsername = "User"
        users.append(User(name: currentUsername))
    }
    
    func increaseCurrentUserScore() {
        if let i = users.firstIndex(where: { $0.name == currentUsername }) {
            users[i].score += 1
        }
    }
    
    func getTracks() {
        isDownloading = true
        NetworkLayer.getTracks()
            .mapError { (error) -> Error in
                print("Error: \(error.localizedDescription)")
                return error
            }
            .map(\.tracks)
            .map { tracks in
                return tracks.map { self.question(for: $0, in: tracks) }
            }
            .sink { _ in }
            receiveValue: {
                [weak self] in
                self?.questions = $0
                self?.questions.forEach {
                    self?.getLyrics(for: $0.track.id)
                }
                self?.objectWillChange.send()
            }
            .store(in: &cancellationTokens)
    }
    
    func getLyrics(for trackID: Int) {
        NetworkLayer.getLyrics(for: trackID)
            .mapError { (error) -> Error in
                print("Error: \(error.localizedDescription)")
                return error
            }
            .sink { _ in }
                receiveValue: {
                    [weak self] lyrics in
                    if let i = self?.questions.firstIndex(where: { $0.track.id == trackID }) {
                        self?.questions[i].track.lyrics = lyrics
                        if let questions = self?.questions, questions.allSatisfy({ $0.track.lyrics != nil }) {
                            self?.isDownloading = false
                        }
                        self?.objectWillChange.send()
                    }
                }
            .store(in: &cancellationTokens)
    }
    
    private func question(for track: Track, in tracks: [Track]) -> Question {
        var answers = [Answer(title: track.artistName, isCorrect: true)]
        var randomArtist: String? {
            return tracks.filter { !answers.map(\.title).contains($0.artistName) }.randomElement()?.artistName
        }
        answers.append(Answer(title: randomArtist ?? "Depeche Mode", isCorrect: false))
        answers.append(Answer(title: randomArtist ?? "Nine Inch Nails", isCorrect: false))
        return Question(track: track, answers: answers.shuffled())
    }
}
